/**
 * Add math op
 * @param a 
 * @param b 
 */
export default  function add(a:number, b: number): number {
    return a + b
}