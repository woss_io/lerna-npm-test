import add from "./inc1";

/**
 * Main function
 */
export async function main(): Promise<{ [k: string]: any }> {
  const sum = add(1, 6);
  return { msg: "YO", sum };
}
